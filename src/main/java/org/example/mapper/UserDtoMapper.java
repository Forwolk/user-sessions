package org.example.mapper;

import org.example.model.dto.UserDto;
import org.example.model.entity.UserEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserDtoMapper {
    UserDto toDto(UserEntity userEntity);

    UserEntity toEntity(UserDto userDto);
}
