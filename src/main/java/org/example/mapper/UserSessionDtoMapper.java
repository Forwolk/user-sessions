package org.example.mapper;

import org.example.model.dto.UserSessionDto;
import org.example.model.entity.UserSessionEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserSessionDtoMapper {

    @Mapping(target = "sessionId", source = "id")
    UserSessionDto toDto(UserSessionEntity userSessionEntity);

    @Mapping(target = "id", source = "sessionId")
    UserSessionEntity toEntity(UserSessionDto userSessionDto);
}
