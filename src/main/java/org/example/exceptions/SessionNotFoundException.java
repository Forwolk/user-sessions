package org.example.exceptions;

public class SessionNotFoundException extends RuntimeException {
    public SessionNotFoundException(Throwable cause) {
        super(cause);
    }
}
