package org.example.exceptions;

public class SaveSessionException extends RuntimeException {
    public SaveSessionException(Throwable cause) {
        super(cause);
    }
}
