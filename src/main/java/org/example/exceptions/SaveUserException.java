package org.example.exceptions;

public class SaveUserException extends RuntimeException {
    public SaveUserException(Throwable cause) {
        super(cause);
    }
}
