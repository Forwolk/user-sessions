package org.example.exceptions;

public class DeleteSessionException extends RuntimeException {
    public DeleteSessionException(Throwable cause) {
        super(cause);
    }
}