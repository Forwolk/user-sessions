package org.example.exceptions;

public class DeleteUserException extends RuntimeException {
    public DeleteUserException(Throwable cause) {
        super(cause);
    }
}
