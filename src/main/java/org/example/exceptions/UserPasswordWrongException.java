package org.example.exceptions;

public class UserPasswordWrongException extends RuntimeException {
    public UserPasswordWrongException(String message) {
        super(message);
    }
}