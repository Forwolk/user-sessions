package org.example.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.example.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;

@ControllerAdvice
public class MyExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserPasswordWrongException.class)
    protected ResponseEntity<MyExceptionDto> handlePasswordWrongException(Exception ex) {
        return new ResponseEntity<>(new MyExceptionDto(403, "password wrong for user: " + ex.getMessage(), ""), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({UserNotFoundException.class, SessionNotFoundException.class})
    protected ResponseEntity<MyExceptionDto> handleNotFoundException(Exception ex) {
        return new ResponseEntity<>(new MyExceptionDto(404, ex.getMessage(), Arrays.toString(ex.getStackTrace())), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({SaveUserException.class, DeleteUserException.class, SaveSessionException.class, DeleteSessionException.class})
    protected ResponseEntity<MyExceptionDto> handleSaveDeleteException(Exception ex) {
        return new ResponseEntity<>(new MyExceptionDto(500, ex.getMessage(), Arrays.toString(ex.getStackTrace())), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RuntimeException.class)
    protected ResponseEntity<MyExceptionDto> handleRuntimeException(Exception ex) {
        return new ResponseEntity<>(new MyExceptionDto(500, ex.getMessage(), Arrays.toString(ex.getStackTrace())), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Data
    @AllArgsConstructor
    private static class MyExceptionDto {
        @JsonProperty("StatusCode")
        Integer statusCode;
        @JsonProperty("StatusDescription")
        String statusDescription;
        @JsonProperty("Exception")
        String exception;
    }
}
