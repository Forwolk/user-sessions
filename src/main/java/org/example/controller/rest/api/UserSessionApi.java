package org.example.controller.rest.api;

import io.swagger.annotations.*;
import org.example.model.dto.UserSessionDto;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;

@Api(value = "UserSessionApi", tags = {"UserSessionApi"})
@Validated
@RequestMapping("/auth")
public interface UserSessionApi {
    @ApiOperation(value = "Получить информацию о пользователе", response = UserSessionDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Отсутствует информация о пользователе")
    })
    @PostMapping("/login")
    ResponseEntity<UserSessionDto> login(
            @ApiParam(value = "Логин пользователя", required = true) @RequestParam(value = "Login") String login,
            @ApiParam(value = "Пароль пользователя", required = true) @RequestParam(value = "Password") String password
    );

    @ApiOperation(value = "Закрыть сессии по sessionId", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Отсутствует информация о сессии")
    })
    @DeleteMapping("/logout?sessionId={sessionId}")
    ResponseEntity<String> logout(
            @ApiParam(value = "Сессия пользователя", required = true) @PathVariable(value = "sessionId") UUID sessionId
    );

    @ApiOperation(value = "Получить все активные сессии пользователя по логину", response = Set.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "Отсутствует активные сессии пользователя")
    })
    @GetMapping("/session/{login}")
    ResponseEntity<Set<UserSessionDto>> getSessionsByLogin(
            @ApiParam(value = "Логин пользователя", required = true) @PathVariable(value = "login") String login
    );
}

