package org.example.controller.rest.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.example.model.dto.UserDto;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Api(value = "UserCrudApi", tags = {"UserCrudApi"})
@Validated
@RequestMapping("/user")
public interface UserCrudApi {

    @ApiOperation(value = "Сохранение пользователя", response = UserDto.class)
    @PostMapping(value = "/save")
    ResponseEntity<UserDto> saveUser(@RequestBody @NotNull @Valid UserDto userDto);

    @ApiOperation(value = "Чтение пользователя", response = UserDto.class)
    @GetMapping(value = "/get/{login}")
    ResponseEntity<UserDto> getUser(
            @ApiParam(value = "Логин пользователя", required = true)
            @PathVariable(value = "login") @NotNull String login);

    @ApiOperation(value = "Удаление пользователя")
    @DeleteMapping(value = "/delete/{login}")
    ResponseEntity<String> deleteUser(
            @ApiParam(value = "Логин пользователя", required = true)
            @PathVariable(value = "login") @NotNull String login);
}
