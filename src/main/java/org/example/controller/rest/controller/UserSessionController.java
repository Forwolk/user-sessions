package org.example.controller.rest.controller;

import lombok.RequiredArgsConstructor;
import org.example.controller.rest.api.UserSessionApi;
import org.example.mapper.UserSessionDtoMapper;
import org.example.model.dto.UserSessionDto;
import org.example.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@RequiredArgsConstructor
@RestController("userSessionController")
public class UserSessionController implements UserSessionApi {

    private final UserService userService;
    private final UserSessionDtoMapper userSessionDtoMapper;

    @Override
    public ResponseEntity<UserSessionDto> login(@NotNull String login, @NotNull String password) {
        return new ResponseEntity<>(userSessionDtoMapper.toDto(userService.login(login, password)), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> logout(UUID sessionId) {
        return new ResponseEntity<>(userService.logout(sessionId), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Set<UserSessionDto>> getSessionsByLogin(String login) {
        return new ResponseEntity<>(userService.getSessionsByLogin(login), HttpStatus.OK);
    }
}
