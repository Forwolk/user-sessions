package org.example.controller.rest.controller;

import lombok.RequiredArgsConstructor;
import org.example.controller.rest.api.UserCrudApi;
import org.example.model.dto.UserDto;
import org.example.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequiredArgsConstructor
public class UserCrudController implements UserCrudApi {

    private final UserService userService;

    @Override
    public ResponseEntity<UserDto> saveUser(@NotNull @Valid UserDto userDto) {
        return new ResponseEntity<>(userService.save(userDto), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<UserDto> getUser(@NotNull String login) {
        return new ResponseEntity<>(userService.getUser(login), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> deleteUser(@NotNull String login) {
        userService.deleteUser(login);
        return new ResponseEntity<>("status=OK", HttpStatus.OK);
    }
}
