package org.example.repository;

import org.example.model.entity.UserEntity;
import org.example.model.entity.UserSessionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

public interface UserSessionRepository extends JpaRepository<UserSessionEntity, UUID> {
    Set<UserSessionEntity> findAllByUser(@NotNull UserEntity userEntity);
    Set<UserSessionEntity> findAllByUserAndTimestampAfterAndExpireBefore(@NotNull UserEntity userEntity, @NotNull Long start, @NotNull Long end);
}
