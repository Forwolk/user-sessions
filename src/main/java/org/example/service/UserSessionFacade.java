package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.exceptions.DeleteSessionException;
import org.example.exceptions.SaveSessionException;
import org.example.exceptions.SessionNotFoundException;
import org.example.model.entity.UserEntity;
import org.example.model.entity.UserSessionEntity;
import org.example.repository.UserSessionRepository;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.Set;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserSessionFacade {
    public static final Integer EXPIRED_PERIOD = 36000;
    public final UserSessionRepository userSessionRepository;

    public UserSessionEntity save(@NotNull UserEntity userEntity) {
        try {
            Long currentTime = System.currentTimeMillis() / 1000L;
            UserSessionEntity userSessionEntity = UserSessionEntity.builder()
                    .user(userEntity)
                    .timestamp(currentTime)
                    .expire(currentTime + EXPIRED_PERIOD)
                    .build();
            return userSessionRepository.save(userSessionEntity);
        } catch (Exception e) {
            throw new SaveSessionException(e);
        }
    }

    public UserSessionEntity findById(@NotNull UUID userSessionId) {
        try {
            return userSessionRepository.findById(userSessionId).get();
        } catch (Exception e) {
            throw new SessionNotFoundException(e);
        }
    }

    public void delete(@NotNull UserSessionEntity userSessionEntity) {
        try {
            userSessionRepository.delete(userSessionEntity);
        } catch (Exception e) {
            throw new DeleteSessionException(e);
        }
    }

    public void deleteAllExpiredSessionByUser(@NotNull UserEntity userEntity) {
        Long curTime = System.currentTimeMillis() / 1000L;
        Long endTime = System.currentTimeMillis() / 1000L + EXPIRED_PERIOD;
        userSessionRepository.findAllByUserAndTimestampAfterAndExpireBefore(userEntity, curTime, endTime)
                .forEach(userSessionRepository::delete);
    }

    public void deleteAllByUser(@NotNull UserEntity userEntity) {
        this.findAllByUser(userEntity)
                .forEach(userSessionRepository::delete);
    }

    public Set<UserSessionEntity> findAllByUser(@NotNull UserEntity userEntity) {
        try {
            return userSessionRepository.findAllByUser(userEntity);
        } catch (Exception e) {
            throw new SessionNotFoundException(e);
        }
    }
}
