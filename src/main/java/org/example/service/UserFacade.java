package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.exceptions.DeleteUserException;
import org.example.exceptions.SaveUserException;
import org.example.exceptions.UserNotFoundException;
import org.example.model.entity.UserEntity;
import org.example.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@RequiredArgsConstructor
@Service
public class UserFacade {
    public final UserRepository userRepository;
    public final UserSessionFacade userSessionFacade;

    public UserEntity getByLogin(@NotNull String userLogin) {
        try {
            UserEntity userEntity = userRepository.findByLogin(userLogin).get();
            userSessionFacade.deleteAllExpiredSessionByUser(userEntity);
            return userEntity;
        } catch (Exception e) {
            throw new UserNotFoundException(e);
        }
    }

    public UserEntity save(@NotNull UserEntity userEntity) {
        try {
            return userRepository.save(userEntity);
        } catch (Exception e) {
            throw new SaveUserException(e);
        }
    }

    public void delete(@NotNull UserEntity userEntity) {
        try {
            userSessionFacade.deleteAllByUser(userEntity);
            userRepository.delete(userEntity);
        } catch (Exception e) {
            throw new DeleteUserException(e);
        }
    }
}
