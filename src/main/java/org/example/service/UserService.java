package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.exceptions.UserPasswordWrongException;
import org.example.mapper.UserDtoMapper;
import org.example.mapper.UserSessionDtoMapper;
import org.example.model.dto.UserDto;
import org.example.model.dto.UserSessionDto;
import org.example.model.entity.UserEntity;
import org.example.model.entity.UserSessionEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Transactional
@RequiredArgsConstructor
@Service
public class UserService {
    private final UserFacade userFacade;
    private final UserSessionFacade userSessionFacade;
    private final UserSessionDtoMapper userSessionDtoMapper;
    private final UserDtoMapper userDtoMapper;

    public UserDto save(@NotNull UserDto userDto) {
        UserEntity userEntity = userFacade.save(userDtoMapper.toEntity(userDto));
        return userDtoMapper.toDto(userEntity);
    }

    public UserDto getUser(@NotNull String login) {
        UserEntity userEntity = userFacade.getByLogin(login);
        return userDtoMapper.toDto(userEntity);
    }

    public void deleteUser(@NotNull String login) {
        UserEntity userEntity = userFacade.getByLogin(login);
        userFacade.delete(userEntity);
    }

    public UserSessionEntity login(@NotNull String login, @NotNull String password) {
        UserEntity userEntity = userFacade.getByLogin(login);
        if (!Objects.equals(password, userEntity.getPassword()))
            throw new UserPasswordWrongException(login);
        return userSessionFacade.save(userEntity);
    }

    public String logout(UUID sessionId) {
        UserSessionEntity sessionEntity = userSessionFacade.findById(sessionId);
        userSessionFacade.delete(sessionEntity);
        return "{“status”=”OK”}";
    }

    public Set<UserSessionDto> getSessionsByLogin(String login) {
        UserEntity userEntity = userFacade.getByLogin(login);
        Set<UserSessionEntity> userSessionEntitySet = userSessionFacade.findAllByUser(userEntity);
        return userSessionEntitySet.stream()
                .map(userSessionDtoMapper::toDto)
                .collect(Collectors.toSet());
    }
}
