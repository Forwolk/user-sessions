package org.example.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.UUID;

@Data
public class UserSessionDto {
    @JsonProperty("SessionId")
    private UUID sessionId;
    @JsonProperty("Timestamp")
    private Long timestamp;
    @JsonProperty("Expire")
    private Long expire;
}
