package org.example.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UserDto {
    @JsonProperty("Login")
    String login;
    @JsonProperty("Password")
    String password;
}
