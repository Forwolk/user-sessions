package org.example;

import org.example.repository.UserRepository;
import org.example.repository.UserSessionRepository;
import org.example.utils.HibernatePhysicalNamingStrategy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackageClasses = {UserRepository.class, UserSessionRepository.class})
@Import({HibernatePhysicalNamingStrategy.class})
@SpringBootApplication
public class TestApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }
}
